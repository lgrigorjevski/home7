
import java.util.*;
//https://github.com/kgalieva/masters/blob/master/term2/tree/huffman/HuffmanTree.java
//https://habr.com/post/144200/
//http://rosettacode.org/wiki/Huffman_coding
/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   private int[] frequencies;

   //code table
   private Map<Byte, Leht> codeTable;

   /** Encoded bits. */
   private int length;

   // TODO!!! Your instance variables here!

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      frequencies = new int[256];
      codeTable = new HashMap<Byte, Leht>();
      buildTree(original);
      // TODO!!! Your constructor here!
   }

   /** Length of encoded data in bits.
    * @return number of bits
    */
   public int bitLength() {
      return length; // TODO!!!
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */

   //saadud sonastiku alusel tolgendame iga symboli 0-ks voi 1-ks
   public byte[] encode (byte [] origData) {
      StringBuilder t = new StringBuilder();
      for (byte a : origData)
         t.append(codeTable.get(a).code);
      length = t.length();
      List<Byte> bytes = new ArrayList<Byte>();
      while (t.length() > 0) {
         while (t.length() < 8)
            t.append('0');
         String str = t.substring(0, 8);
         bytes.add((byte) Integer.parseInt(str, 2));
         t.delete(0, 8);
      }
      byte[] result = new byte[bytes.size()];
      for (int i = 0; i < bytes.size(); i++)
         result[i] = bytes.get(i);
      return result;
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */

   //sonastiku aluse otsime mis on kodeeritud
   public byte[] decode (byte[] encodedData) {
      StringBuilder code = new StringBuilder();
      for (int i = 0; i < encodedData.length; i++)
         code.append(String.format("%8s", Integer.toBinaryString(encodedData[i] & 0xFF)).replace(' ', '0'));
      String str = code.substring(0, length);
      List<Byte> bytes = new ArrayList<Byte>();
      String code2 = "";
      while (str.length() > 0) {
         code2 += str.substring(0, 1);
         str = str.substring(1);
         Iterator<Leht> list = codeTable.values().iterator();
         while (list.hasNext()) {
            Leht leht = list.next();
            if (leht.code.equals(code2)) {
               bytes.add(leht.value);
               code2 = "";
               break;
            }
         }
      }
      byte[] result = new byte[bytes.size()];
      for (int i = 0; i < bytes.size(); i++)
         result[i] = bytes.get(i);
      return result;
   }

   /** Main method. */
   public static void main (String[] params) {
      //symbolid -> byte-massiivi, iga symbol omab oma byte-koodi
      String tekst = "1112234";
      byte[] orig = tekst.getBytes();
      //tekitame keskkonda kus on kodeerimine ning dekodeerimine
      //selleks meil on vaja teada kui palju kordi on yks voi teine symbol tekstis olnud
      Huffman huf = new Huffman (orig);
      //kodeerime huffman'isse
      byte[] kood = huf.encode (orig);
      //dekodeerime huffman'i koodist
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      //System.out.println(Arrays.toString(orig));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
   }

   abstract class HuffmanTree implements Comparable<HuffmanTree> {


      public final int frequency;

      public HuffmanTree(int frequency) {
         this.frequency = frequency;
      }

      @Override
      public int compareTo(HuffmanTree tree) {
         return frequency - tree.frequency;
      }
   }

   class Leht extends HuffmanTree {

      public final byte value;

      public String code;

      public Leht(int frequency, byte value) {
         super(frequency);
         this.value = value;

      }

   }

   class HuffmanNode  extends HuffmanTree {

      public final HuffmanTree left;
      public final HuffmanTree right;


      public HuffmanNode (HuffmanTree left, HuffmanTree right) {
         super(left.frequency + right.frequency);
         this.left = left;
         this.right = right;
      }

   }

   private void buildTree(byte[] data) {
      //kui palju kordi on yks voi teine symbol tekstis olnud
      for (byte a : data)
         frequencies[a]++;
      System.out.println(Arrays.toString(data));
      PriorityQueue<HuffmanTree> trees = new PriorityQueue<HuffmanTree>();
      for (int i = 0; i < frequencies.length; i++)
         if (frequencies[i] > 0)
            trees.offer(new Leht(frequencies[i], (byte) i));
      //sorditud vastavalt sellest kui tihti on symbol olnud (nn "lehtede"-viisil)
      assert trees.size() > 0;
      //tsuktiga votame kahe lehe kaupa ja yhendame node'sse kuni jaab 1 element

      while (trees.size() > 1)
         trees.offer(new HuffmanNode(trees.poll(), trees.poll()));
      //siin kasutame viimane element mis pani ennasse kokku koik elemendid
      HuffmanTree tree = trees.poll();
      //kodeerime sellest huffman koodi
      printCodes(tree, new StringBuilder());
   }

   private void printCodes(HuffmanTree tree, StringBuilder prefix) {
      //kontroll et ei ole tyhi
      assert tree != null;
      //toodeldame lehe ja valmis
      if (tree instanceof Leht) {
         Leht leaf = (Leht)tree;
         System.out.println(leaf.value + "\t" + leaf.frequency + "\t" + prefix);
         leaf.code = (prefix.length() > 0) ? prefix.toString() : "0";
         codeTable.put(leaf.value, leaf);
      } else if (tree instanceof HuffmanNode) {
         //nyyd tagasi node'isse
         //tree kina ei tea kas node voi leht
         HuffmanNode node = (HuffmanNode) tree;
         //vasak ehk esimene side kodeeritakse nulliga
         //rekursiivselt toodeldame edasi, loigates maha yhe elemendi
         printCodes(node.left,  prefix.append('0'));
         prefix.deleteCharAt(prefix.length() -1);
         // traverse right
         printCodes(node.right, prefix.append('1'));
         prefix.deleteCharAt(prefix.length() -1);
      }
   }
}